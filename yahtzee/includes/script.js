var roll = 0;
var eyes = 0;
var i;

function rollDice(roll)
{
	var goOn = true;

	var allEyes = [];
	
	if(roll == 3)
	{
		$('#roller').hide();
	}
	else
	{
		$('#roller').html('Roll dice');
	}

	if(roll > 3)
	{
		$('.dice').each(function(){
			$(this).removeClass('hold');
		});
		
		roll = 1;
	}
	
	$('.dice').each(function(){
		if(roll == 1)
		{
			$(this).removeClass('hold');
		}
	
		if(!$(this).hasClass('hold'))
		{
			eyes = Math.floor((Math.random() * 6) + 1);
			$(this).text(eyes);
		}
	});
	
	$('.dice').each(function(){
		allEyes.push(parseInt($(this).text()));
	});
	
	$('#rolls').text(roll);
	
	calculatePoints(allEyes);

}

function calculatePoints(allEyes)
{
	calculateOnes(allEyes);
	calculateTwos(allEyes);
	calculateThrees(allEyes);
	calculateFours(allEyes);
	calculateFives(allEyes);
	calculateSixes(allEyes);
	
	calculateThreeOfAKind(allEyes);
	calculateFourOfAKind(allEyes);
	calculateFullHouse(allEyes);
	calculateSmallStraight(allEyes);
	calculateLargeStraight(allEyes);
	calculateYahtzee(allEyes);
	calculateChance(allEyes);
	
	calculateAll();
}

function calculateOnes(allEyes)
{
	if(!$('#ones').hasClass('frozen'))
	{
		var sumOnes = 0;
		for (i = 0; i < allEyes.length; i++) {
			if(allEyes[i] == 1)
			{
				sumOnes += 1;
			}
		}
		$('#ones span').text(sumOnes);
	}
}

function calculateTwos(allEyes)
{
	if(!$('#twos').hasClass('frozen'))
	{
		var sumTwos = 0;
		for (i = 0; i < allEyes.length; i++) {
			if(allEyes[i] == 2)
			{
				sumTwos += 2;
			}
		}
		$('#twos span').text(sumTwos);
	}
}

function calculateThrees(allEyes)
{
	if(!$('#threes').hasClass('frozen'))
	{
		var sumThrees = 0;
		for (i = 0; i < allEyes.length; i++) {
			if(allEyes[i] == 3)
			{
				sumThrees += 3;
			}
		}
		$('#threes span').text(sumThrees);
	}
}

function calculateFours(allEyes)
{
	if(!$('#fours').hasClass('frozen'))
	{
		var sumFours = 0;
		for (i = 0; i < allEyes.length; i++) {
			if(allEyes[i] == 4)
			{
				sumFours += 4;
			}
		}
		$('#fours span').text(sumFours);
	}
}

function calculateFives(allEyes)
{
	if(!$('#fives').hasClass('frozen'))
	{
		var sumFives = 0;
		for (i = 0; i < allEyes.length; i++) {
			if(allEyes[i] == 5)
			{
				sumFives += 5;
			}
		}
		$('#fives span').text(sumFives);
	}
}

function calculateSixes(allEyes)
{
	if(!$('#sixes').hasClass('frozen'))
	{
		var sumSixes = 0;
		for (i = 0; i < allEyes.length; i++) {
			if(allEyes[i] == 6)
			{
				sumSixes += 6;
			}
		}
		$('#sixes span').text(sumSixes);
	}
}

function calculateThreeOfAKind(allEyes)
{
	if(!$('#three-of-a-kind').hasClass('frozen'))
	{
		var sumThreeOfAKind = 0;
		if(count(allEyes, 1) >= 3 || count(allEyes, 2) >= 3 || count(allEyes, 3) >= 3 || count(allEyes, 4) >= 3 || count(allEyes, 5) >= 3 || count(allEyes, 6) >= 3)
		{
			var sumThreeOfAKind = 0;
			for (i = 0; i < allEyes.length; i++) {
				sumThreeOfAKind += allEyes[i];
			}
		}
		$('#three-of-a-kind span').text(sumThreeOfAKind);
	}
}
function calculateFourOfAKind(allEyes)
{
	if(!$('#four-of-a-kind').hasClass('frozen'))
	{
		var sumFourOfAKind = 0;
		if(count(allEyes, 1) >= 4 || count(allEyes, 2) >= 4 || count(allEyes, 3) >= 4 || count(allEyes, 4) >= 4 || count(allEyes, 5) >= 4 || count(allEyes, 6) >= 4)
		{
			for (i = 0; i < allEyes.length; i++) {
				sumFourOfAKind += allEyes[i];
			}
		}
		$('#four-of-a-kind span').text(sumFourOfAKind);
	}
}
function calculateFullHouse(allEyes)
{
	if(!$('#full-house').hasClass('frozen'))
	{
		var sumFullHouse = 0;
		if((count(allEyes, 1) == 3 || count(allEyes, 2) == 3 || count(allEyes, 3) == 3 || count(allEyes, 4) == 3 || count(allEyes, 5) == 3 || count(allEyes, 6) == 3) && (count(allEyes, 1) == 2 || count(allEyes, 2) == 2 || count(allEyes, 3) == 2 || count(allEyes, 4) == 2 || count(allEyes, 5) == 2 || count(allEyes, 6) == 2))
		{
			for (i = 0; i < allEyes.length; i++) {
				sumFullHouse += allEyes[i];
			}
		}
		$('#full-house span').text(sumFullHouse);
	}
}
function calculateSmallStraight(allEyes)
{
	if(!$('#small-straight').hasClass('frozen'))
	{
		var sumSmall = 0;
	
		allEyes.sort();
		
		/*var arr1 = [1,2,3,4];
		var arr2 = [2,3,4,5];
		var arr3 = [3,4,5,6];*/

		if((jQuery.inArray(3, allEyes) !== -1 && jQuery.inArray(4, allEyes) !== -1) && ((jQuery.inArray(1, allEyes) !== -1 && jQuery.inArray(2, allEyes) !== -1) || (jQuery.inArray(2, allEyes) !== -1 && jQuery.inArray(5, allEyes) !== -1) || (jQuery.inArray(5, allEyes) !== -1 && jQuery.inArray(6, allEyes) !== -1)))
		{
			sumSmall = 30;
		}
		
		$('#small-straight span').text(sumSmall);
	}
}
function calculateLargeStraight(allEyes)
{
	if(!$('#large-straight').hasClass('frozen'))
	{
		var sumLarge = 0;
	
		allEyes.sort();
		
		var arr1 = [1,2,3,4,5];
		var arr2 = [2,3,4,5,6];
		
		if (($(allEyes).not(arr1).length == 0 && $(arr1).not(allEyes).length == 0 && allEyes.length == arr1.length) || ($(allEyes).not(arr2).length == 0 && $(arr2).not(allEyes).length == 0 && allEyes.length == arr2.length))
		{
			sumLarge = 40;
		}

		$('#large-straight span').text(sumLarge);
	}
}
function calculateYahtzee(allEyes)
{
	if(!$('#yahtzee').hasClass('frozen'))
	{
		var yahtzee = true;
		var sumYahtzee = 0;

		for(i = 1; i < allEyes.length; i++)
		{
			if(allEyes[i] != allEyes[i-1])
			{
				yahtzee = false;
				return false;
			}
		}
		
		if(yahtzee == true)
		{
			sumYahtzee = 50;
		}

		$('#yahtzee span').text(sumYahtzee);
	}
}
function calculateChance(allEyes)
{
	if(!$('#chance').hasClass('frozen'))
	{
		var sumChance = 0;
		for (i = 0; i < allEyes.length; i++) {
			sumChance += allEyes[i];
		}
		$('#chance span').text(sumChance);
	}
}

function calculateAll()
{
	var allPoints = 0;
	
	$('.winField').each(function(){
		if($(this).hasClass('freeze') || $(this).hasClass('frozen'))
		{
			allPoints += parseInt($(this).find('span').text());
		}
	});
	
	$('#points span').text(allPoints);
}

function count(array, value) {
	var counter = 0;
	for(var i=0;i<array.length;i++)
	{
		if (array[i] === value)
		{
			counter++;
		}
	}
	return counter;
}

function setNewGame()
{
	$('.winField').each(function(){
		$(this).removeClass('frozen');
		$(this).removeClass('freeze');
		$(this).find('span').text("0");
	});
	
	calculateAll();
}

$(document).ready(function($){
	$('#roller').click(function(){
	
		roll++;
	
		$('.winField').each(function(){
			if($(this).hasClass('freeze'))
			{
				$(this).removeClass('freeze');
				$(this).addClass('frozen');
				roll = 1;
			}
		});

		if(roll > 3)
		{
			roll = 1;
		}

		rollDice(roll);
	});
	
	$('.dice').click(function(){
		if(roll != 0 && roll != 3)
		{
			$(this).toggleClass('hold');
		}
	});
	
	$('.winField').click(function(){
		if(roll > 0 && roll < 4)
		{
			var action = 0;
		
			if($(this).hasClass('freeze'))
			{
				action = 0;
			}
			else
			{
				action = 1;
			}
		
			$('.winField').each(function(){
				$(this).removeClass('freeze');
			});

			if(action == 0)
			{
				$(this).removeClass('freeze');
			}
			else
			{
				$(this).addClass('freeze');
			}
		}
		
		if(roll == 3)
		{
			if($(this).hasClass('freeze'))
			{
				$('#roller').html('Next round, please');
				$('#roller').show();
			}
			else
			{
				$('#roller').hide();
			}
		}
		
		var goOn = false;
		
		$('.winField').each(function(){
			if(!$(this).hasClass('frozen') && !$(this).hasClass('freeze'))
			{
				goOn = true;
			}
		});
		
		calculateAll();
		
		if(goOn == false)
		{
			$('#roller').html('Start a new game!');
			
			$('#roller').click(function(){
				setNewGame();
			});
		}
	});
});